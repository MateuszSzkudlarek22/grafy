module com.example.java2022 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.java2022 to javafx.fxml;
    exports com.example.java2022;
}