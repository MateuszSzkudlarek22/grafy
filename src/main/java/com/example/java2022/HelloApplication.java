package com.example.java2022;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    double X;
    double Y;
    @Override
    public void start(Stage stage) throws IOException{
        Scene scene = new Scene(Functions.directedScene());
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.setTitle("Aplikacja do wizualizacji grafów");
        stage.show();
    }
    public static void main(String[] args) {
        launch();
    }
}