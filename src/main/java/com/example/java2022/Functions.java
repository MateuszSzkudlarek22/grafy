package com.example.java2022;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.*;
import javafx.scene.paint.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.*;

import java.util.HashMap;

public class Functions {
    static boolean isNumeric(String n){
        try {
            Integer.parseInt(n);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    static Pane directedScene() {
        CheckBox box = new CheckBox("Poprawne kolorowanie");
        box.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(box.isSelected()){
                    colourGraph();
                    Graph.changeMode = false;
                    Graph.Flag = null;
                }
                else{
                    Graph.changeMode = true;
                    normalGraph();
                }
            }
        });
        box.setLayoutX(1);
        box.setLayoutY(1);
        Pane sp = new Pane();
        ContextMenu cm = Utils.sceneAddContextMenu(sp);
        sp.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {
                if(Graph.changeMode) {
                    Graph.X = contextMenuEvent.getSceneX();
                    Graph.Y = contextMenuEvent.getSceneY();
                    cm.show(sp.getScene().getWindow(), contextMenuEvent.getSceneX(), contextMenuEvent.getSceneY());
                }
            }
        });
        sp.getChildren().add(box);
        return sp;
    }

    static boolean isNumberOkay(int colors[], int m, int v, HashMap<Integer, Integer> map, HashMap<Integer, Integer> map2){
        if(v==Graph.graph.size+1){
            if(isColorOkay(colors, map, map2)) return true;
            return false;
        }
        for(int i=0; i<m; i++){
            colors[v] = i;
            if(isNumberOkay(colors, m, v+1, map, map2)) return true;
        }
        return false;
    }

    static void normalGraph(){
        for(Button b: Graph.biMap.keySet()){
            b.setStyle("-fx-background-color: #FF0000;  -fx-border-color: #000000; ");
        }
    }

    static void colourGraph(){
        int colors[]=null;
        HashMap<Integer, Integer> iimap = new HashMap<>();
        HashMap<Integer, Integer> iimap2 = new HashMap<>();
        int k=1; //popraw pętle
        for(Integer i: Graph.biMap.values()){
            iimap.put(k, i);
            iimap2.put(i, k);
            k++;
        }
        int l=1;
        int r = Graph.graph.size+1;
        while(l<r){
            int t[] = new int[Graph.graph.size+1];
            int s = (l+r)/2;
            if(isNumberOkay(t, s, 1, iimap, iimap2)){
                r = s;
                colors = t;
            }
            else l = s+1;
        }
        if(l<=25){
            for(Button b:Graph.biMap.keySet()){
                b.setStyle("-fx-border-color: #000000;  -fx-background-color: "+Colors.colors[colors[iimap2.get(Graph.biMap.get(b))]]+";");
            }
        }
        else{
            Utils.errorStage("Liczba chromatyczna grafu: "+l).show();
        }
    }

    static boolean isColorOkay(int t[], HashMap<Integer, Integer> map, HashMap<Integer, Integer> map2){
        for(int v=1; v<=Graph.graph.size; v++) {
            for (int i : Graph.graph.graph.get(map.get(v))) {
                if (t[map2.get(i)] == t[v]) return false;
            }
        }
        return true;
    }
}

class Colors{
    static String[] colors = {
            "#C0C0C0",
            "#FFFF00",
            "#FFFFFF",
            "#00FF00",
            "#FF0000",
            "#FFA500",
            "#FF00FF",
            "#008000",
            "#00FF00",
            "#808000",
            "#FFFF00",
            "#000080",
            "#0000FF",
            "#008080",
            "#00FFFF",
            "#8a2be2",
            "#a52a2a",
            "#deb887",
            "#d2691e",
            "#dc143c",
            "#00008b",
            "#8b008b",
            "#ff8c00",
            "#e9967a",
            "#a0522d"
    };
}