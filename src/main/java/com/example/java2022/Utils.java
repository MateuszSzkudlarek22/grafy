package com.example.java2022;

import javafx.event.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.*;
import javafx.util.Pair;
import javafx.scene.effect.*;

import java.util.*;

public class Utils {
    static Line getLine(Button n1, Button n2, Pane sp){
        Line l = new Line();
        Graph.lbMap.put(l, new Pair<>(n1, n2));
        Graph.blMap.get(n1).add(new Pair<>(l, "Start"));
        Graph.blMap.get(n2).add(new Pair<>(l, "End"));
        l.setStartX(n1.getLayoutX()+20);
        l.setStartY(n1.getLayoutY()+20);
        l.setEndX(n2.getLayoutX()+20);
        l.setEndY(n2.getLayoutY()+20);
        l.setStrokeWidth(8);
        l.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {
                if(Graph.changeMode) {
                    removeLineContextMenu(l, sp).show(sp.getScene().getWindow(), contextMenuEvent.getSceneX(), contextMenuEvent.getSceneY());
                    contextMenuEvent.consume();
                }
            }
        });
        n1.toFront();
        n2.toFront();
        return l;
    }
    static Button getVortex(Pane sp, String n, double X, double Y){
        Button bt = new Button(n);
        bt.setLayoutX(X);
        bt.setLayoutY(Y);
        bt.setShape(new Circle(20));
        bt.setMaxSize(40, 40);
        bt.setMinSize(40, 40);
        bt.setStyle("-fx-background-color: #FF0000;  -fx-border-color: #000000; ");
        bt.toFront();
        Blend blend = new Blend(BlendMode.COLOR_BURN);
        bt.setEffect(blend);
        Graph.biMap.put(bt, Integer.parseInt(n));
        Graph.blMap.put(bt, new LinkedList<>());
        bt.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if((mouseEvent.getSceneX()-20<=170 ? mouseEvent.getSceneY()-20>=20 : mouseEvent.getSceneY()-20>=0) &&
                        (mouseEvent.getSceneY()-20<=20 ? mouseEvent.getSceneX()-20>=170 : mouseEvent.getSceneX()-20>=0)) {
                    bt.setLayoutX(mouseEvent.getSceneX()-20);
                    bt.setLayoutY(mouseEvent.getSceneY()-20);
                    if (Graph.blMap.get(bt) != null) {
                        for (Pair<Line, String> y : Graph.blMap.get(bt)) {
                            if (y.getValue() == "Start") {
                                y.getKey().setStartX(mouseEvent.getSceneX());
                                y.getKey().setStartY(mouseEvent.getSceneY());
                            } else {
                                y.getKey().setEndX(mouseEvent.getSceneX());
                                y.getKey().setEndY(mouseEvent.getSceneY());
                            }
                        }
                    }
                }
                bt.toFront();
                mouseEvent.consume();
            }
        });
        bt.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton() == MouseButton.PRIMARY && mouseEvent.getClickCount()==2 && Graph.changeMode) {
                    if (Graph.Flag == null) {
                        Graph.Flag = bt;
                        bt.setStyle("-fx-background-color: #0000FF; -fx-border-color: #000000");
                    }
                    else {
                        if (Graph.Flag != bt) {
                            Integer n1 = Graph.biMap.get(Graph.Flag);
                            Integer n2 = Integer.parseInt(n);
                            if (Graph.graph.containsEdge(n1, n2)) {
                                errorStage("Krawędź już istnieje.").show();
                            } else {
                                Graph.graph.addEdge(n1, n2);
                                Line l = getLine(bt, Graph.Flag, sp);
                                sp.getChildren().add(l);
                                bt.toFront();
                                Graph.Flag.toFront();
                            }
                        }
                        Graph.Flag.setStyle("-fx-background-color: #FF0000; -fx-border-color: #000000");
                        Graph.Flag = null;
                    }
                }
            }
        });
        bt.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {
                if(Graph.changeMode) {
                    removeVortexContextMenu(bt, sp).show(sp.getScene().getWindow(), contextMenuEvent.getSceneX(), contextMenuEvent.getSceneY());
                    contextMenuEvent.consume();
                }
            }
        });
        return bt;
    }

    static Stage errorStage(String n){
        Stage s = new Stage();
        Text text = new Text(n);
        StackPane sp = new StackPane();
        sp.getChildren().add(text);
        s.setScene(new Scene(sp, 300, 200));
        return s;
    }

    static Stage addVortexStage(Pane sp, double X, double Y){
        Stage s = new Stage();
        GridPane gp = new GridPane();
        TextField tf1 = new TextField();
        Text vortex = new Text("Numer wierzchołka");
        Button add = new Button("Dodaj wierzchołek");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String n = tf1.getText();
                if(n=="" ||  !Functions.isNumeric(n)){
                    errorStage("Parametry są niepoprawne.").show();
                    s.close();
                    return;
                }
                Integer a = Integer.parseInt(n);
                if(Graph.graph.containsVertex(a)){
                    errorStage("Wierzchołek o podanym numerze już istnieje").show();
                    s.close();
                    return;
                }
                else{
                    Graph.graph.addVertex(a);
                    Button b = getVortex(sp, n, X, Y);
                    sp.getChildren().add(b);
                }
                s.close();
            }
        });
        gp.addRow(0, vortex, tf1);
        gp.addRow(1, add);
        s.setScene(new Scene(gp, 300, 200));
        return s;
    }
    static void removeEdge(Line line, Pane sp){
        Pair<Button, Button> pair = Graph.lbMap.get(line);
        List<Pair<Line, String>> list1 = Graph.blMap.get(pair.getKey());
        List<Pair<Line, String>> list2 = Graph.blMap.get(pair.getValue());
        Pair<Line, String> r1 = null;
        Pair<Line, String> r2 = null;
        for(Pair<Line, String> p: list1){
            if(p.getKey() == line){
                r1 = p;
                break;
            }
        }
        for(Pair<Line, String> p: list2){
            if(p.getKey() == line){
                r2 = p;
                break;
            }
        }
        list1.remove(r1);
        list2.remove(r2);
        Graph.lbMap.remove(line);
        Integer n1 = Graph.biMap.get(pair.getKey());
        Integer n2 = Graph.biMap.get(pair.getValue());
        Graph.graph.removeEdge(n1, n2);
        sp.getChildren().remove(line);
    }
    static void removeVortex(Button b, Pane sp){
        Graph.graph.removeVortex(Graph.biMap.get(b));
        Graph.biMap.remove(b);
        List<Pair<Line, String>> list = Graph.blMap.get(b);
        LinkedList<Pair<List<Pair<Line, String>>, Pair<Line, String>>> toRemove = new LinkedList<>();
        for(Pair<Line, String> p: list){
            Line line = p.getKey();
            Pair<Button, Button> pair = Graph.lbMap.get(line);
            List<Pair<Line, String>> list1 = Graph.blMap.get(pair.getKey());
            List<Pair<Line, String>> list2 = Graph.blMap.get(pair.getValue());
            Pair<Line, String> r1 = null;
            Pair<Line, String> r2 = null;
            for(Pair<Line, String> p1: list1){
                if(p1.getKey() == line){
                    r1 = p1;
                    break;
                }
            }
            for(Pair<Line, String> p2: list2){
                if(p2.getKey() == line){
                    r2 = p2;
                    break;
                }
            }
            toRemove.add(new Pair<List<Pair<Line, String>>, Pair<Line, String>>(list1, r1));
            toRemove.add(new Pair<List<Pair<Line, String>>, Pair<Line, String>>(list2, r2));
            Graph.lbMap.remove(line);
            sp.getChildren().remove(line);
        }
        toRemove.forEach(pair -> pair.getKey().remove(pair.getValue()));
        Graph.blMap.remove(b);
        sp.getChildren().remove(b);
    }
    static ContextMenu removeVortexContextMenu(Button b1, Pane sp){
        ContextMenu cm = new ContextMenu();
        MenuItem remove = new MenuItem("Usuń wierzchołek");
        remove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                removeVortex(b1, sp);
            }
        });
        cm.getItems().add(remove);
        return cm;
    }
    static ContextMenu removeLineContextMenu(Line line, Pane sp){
        ContextMenu cm = new ContextMenu();
        MenuItem remove = new MenuItem("Usuń krawędź");
        remove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                removeEdge(line, sp);
            }
        });
        cm.getItems().add(remove);
        return cm;
    }
    static ContextMenu sceneAddContextMenu(Pane sp){
        ContextMenu cm = new ContextMenu();
        MenuItem add = new MenuItem("Dodaj wierzchołek");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                addVortexStage(sp, Graph.X, Graph.Y).show();
            }
        });
        cm.getItems().add(add);
        return cm;
    }
}
