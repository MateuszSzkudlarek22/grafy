package com.example.java2022;


import javafx.scene.control.*;
import javafx.scene.shape.*;
import javafx.util.*;

import java.util.*;


public class Graph{
    static MyGraph graph = new MyGraph();
    static Button Flag = null;
    static boolean changeMode = true;
    static HashMap<Button, Integer> biMap = new HashMap<>();
    static HashMap<Button, List<Pair<Line, String>>> blMap = new HashMap<>();
    static HashMap<Line, Pair<Button, Button>> lbMap = new HashMap<>();
    static double X;
    static double Y;
}

class MyGraph {
    HashMap<Integer, List<Integer>> graph;
    HashMap<Integer, Integer> iimap;

    public static int size = 0;

    MyGraph(){
        graph= new HashMap<>();
        iimap = new HashMap<>();
    }


    public void addVertex(Integer t) {
        if(!graph.containsKey(t)){
            graph.put(t, new LinkedList<Integer>());
        }
        size++;
    }

    public boolean containsVertex(Integer t) {
        return graph.containsKey(t);
    }

    public void addEdge(Integer t, Integer s) {
        graph.get(t).add(s);
        graph.get(s).add(t);
    }

    public void removeEdge(Integer t, Integer s) {
        graph.get(t).remove(s);
        graph.get(s).remove(t);
    }

    public void removeVortex(Integer t){
        for(Integer i : graph.keySet()){
            graph.get(i).remove(t);
        }
        graph.remove(t);
        size--;
    }

    public boolean containsEdge(Integer t, Integer s) {
        if(graph.containsKey(t) && graph.containsKey(s)){
            return graph.get(t).contains(s);
        }
        return false;
    }
}